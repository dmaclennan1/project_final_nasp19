#!/bin/bash

source ~/project_final_nasp19/router/config.sh
source ~/project_final_nasp19/router/arrays.sh

echo "Initiating first time setup.."
touch "/var/log/router-${cdate}"
echo "Log file created in /var/log/router-${cdate}"

hostnamectl set-hostname rtr.s${studentid}.as.learn
echo "Host name has been configured.."

echo "Setting up network interfaces.."
bash ~/project_final_nasp19/router/interface-conf/setup.sh
echo "Networking interfaces configured.."

echo "Installing netdev-bootstrap.."
bash ~/project_final_nasp19/router/netdev-bootstrap.sh


echo "Setting up routing services.."
bash ~/project_final_nasp19/router/services-conf/quagga.service/setup.sh
bash ~/project_final_nasp19/router/services-conf/quagga.service/ospf.service/setup.sh
bash ~/project_final_nasp19/router/services-conf/quagga.service/zebra.service/setup.sh

echo "Setting up dhcpd.services.."
bash ~/project_final_nasp19/router/services-conf/dhcpd.service/setup.sh

echo "Setting up dns services.."
bash ~/project_final_nasp19/router/services-conf/dns.service/nsd.service/setup.sh
bash ~/project_final_nasp19/router/services-conf/dns.service/unbound.service/setup.sh

echo "Setting up hostapd.services.."
bash ~/project_final_nasp19/router/services-conf/hostapd.service/setup.sh

echo "Router configuration complete!"
echo "Log file located in /var/log/router-${cdate}.."
echo " —------'
       \   ^__^'
        \  (oo)\_______'
           (__)\       )\/\'
               ||----w |'
               ||     ||'"
