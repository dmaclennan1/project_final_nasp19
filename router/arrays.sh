#!/bin/bash

###################################################
# -------------- #NSD Arrays# --------------       #
###################################################

declare -A -x ZONES1=(                        #Zone name declaration
[A]=s${studentid}.${intdom}
[PTR]=${studentid}.16.10.in-addr.arpa
)

###################################################
# ---------  - #Unbound Arrays# -  ---------       #
###################################################
declare -A -x stubzone1=(
[FW]="s${studentid}.${intdom}"
[PTR]="${studentid}.16.1.in-addr.arpa"
)

declare -A -x ubforward=(
[1]=${forwarddom1}
[2]=${forwarddom2}
[3]=${forwarddom3}
)

declare -A -x ubforaddr=(
[1]=${forwardaddress1}
[2]=${forwardaddress2}
[3]=${forwardaddress3}
[4]=${forwardaddress4}
)

###################################################
# ----------- #Interface Arrays# -----------       #
###################################################

declare -A -x EXTERNAL=(                    #External Interface Settings
[INTERFACE]=${ext_if}
[HOST]=${extnetwork}.${exthost}
[NETWORK]=${extnetwork}.0
[PREFIX]=${ext_cidr} )

declare -A -x LAN=(                          #Internal LAN Interface
[INTERFACE]=${lan_if}
[HOST]=${lannetwork}.${lanhost}
[NETWORK]=${lannetwork}.0
[PREFIX]=${lan_cidr} )

declare -A -x WLAN=(                         #Wireless LAN Interface
[INTERFACE]=${wlan_if}
[HOST]=${wlannetwork}.${wlanhost}
[NETWORK]=${wlannetwork}.128
[PREFIX]=${wlan_cidr} )
