#!/bin/bash
###################################################
#     IN CONFIGURATION FILE FOR ROUTER            #
#      #-----EDIT AS NECESSARY-----#              #
###################################################

###################################################
#  --------#Preconfiguration Variables# --------  #
###################################################
declare cdate=`date +%Y-%m-%d:%H:%M`
declare studentid='12'
declare intdom='as.learn'
declare ext_if='eth0'
declare lan_if='eth1'
declare wlan_if='wlp0s6u1'

declare ext_cidr='24'
declare extnetmask='255.255.255.0'
declare extnetwork='10.16.255'
declare exthost="${studentid}"

declare lan_cidr='25'
declare lannetmask='255.255.255.128'
declare lannetwork="10.16.${studentid}"
declare lanhost="126"

declare wlan_cidr='25'
declare wlannetmask='255.255.255.128'
declare wlannetwork="10.16.${studentid}"
declare wlanhost="254"

#Mail Server Settings
declare mailmacaddr='08:00:27:08:89:A0'
declare mail_addr='10.16.12.1'

#Specialized Wireless Lan Settings
declare wlan_channel='11'
declare wlan_mode='Master'
declare wlan_type='Wireless'
declare wlan_essid="NASP19_${studentid}"

###################################################
# ---------- #HostAPD Configuration# ---------- #
###################################################

declare hapdriver='nl80211'
declare hapmode='g'
declare hapessid="$wlan_essid"                   #This is reflected by the wireless interface configuration
declare hapchannel="$wlan_channel"            #This is reflected by the wireless interface configuration
declare hapif="$wlan_if"                         #This is reflected by the wireless interface configuration
declare hauth='1'

###################################################
# ------------- #NSD Configuration# ------------- #
###################################################

declare nsdint=${extnetwork}.${exthost}                   #Interface Settings
declare ipv4listen="yes"                           #IP Listen states
declare ipv6listen="no"
declare nsdport="53"                               #Default port binding


###################################################
# ----------- #Unbound Configuration# ----------- #
###################################################

declare ubface=${lannetwork}.${lanhost}
declare unboundport="53"
declare ipv4access="yes"
declare ipv6access="no"
declare wlanaccess="allow"
declare lanaccess="allow"
declare extaccess="allow"
declare moduleconfig='iterator'
declare stubaddr=${extnetwork}.${exthost}
declare forwarddom1='learn'
declare forwarddom2='htp.bcit.ca'
declare forwarddom3='bcit.ca'
declare forwardaddress1='142.232.221.253'
declare forwardaddress2='142.232.221.253'
declare forwardaddress3='142.232.221.253'
declare forwardaddress4='142.232.221.253'

###################################################
# ---------- #VirtualBox Configuration# --------- #
###################################################

declare rtrname='rtr-test'                #VirtualBox Filename
declare os='RedHat'                       #Virtualbox OS Preference
declare osloc='/home/dmaclennan/Desktop/HDD/ISOs/Linux/centos-64-min.iso' #ISO file location
declare guestadditions='/usr/share/virtualbox/VBoxGuestAdditions.iso' #Virtualbox Guest addition location
declare vboxram='2048'                    #Virtual Memory for Server
declare vboxvram='128'                    #Virtual Video Memory for Server
declare controller='IDE Controller'       #Controller for virtual disks
declare studentvlan='vlan2012'            #VLAN network provided by NASP19
declare vboxnictype1='virtio'             #Network Interface Type [Defaulted to paravirtualized]
declare internalnet='as_net'              #Primary internal network as specified by NASP19
declare vboxnictype2='virtio'             #Network Interface Type [Defaulted to paravirtualized]
declare macaddr1=
declare macaddr2=
