source ~/project_final_nasp19/router/config.sh
source ~/project_final_nasp19/router/arrays.sh

#Copies template and renames each one respectively as provided by config values.
cp ~/project_final_nasp19/router/interface-conf/ifcfg-temp /etc/sysconfig/network-scripts/ifcfg-${ext_if}
cp ~/project_final_nasp19/router/interface-conf/ifcfg-temp /etc/sysconfig/network-scripts/ifcfg-${lan_if}
cp ~/project_final_nasp19/router/interface-conf/ifcfg-temp /etc/sysconfig/network-scripts/ifcfg-${wlan_if}

#Replaces values in template files with values provided in config file.

sed -i "s/INTERFACEV/${EXTERNAL[INTERFACE]}/g" /etc/sysconfig/network-scripts/ifcfg-${ext_if}
sed -i "s/INTERFACEV/${LAN[INTERFACE]}/g" /etc/sysconfig/network-scripts/ifcfg-${lan_if}
sed -i "s/INTERFACEV/${WLAN[INTERFACE]}/g" /etc/sysconfig/network-scripts/ifcfg-${wlan_if}

sed -i "s/NETWORKADDR/${EXTERNAL[NETWORK]}/g" /etc/sysconfig/network-scripts/ifcfg-${ext_if}
sed -i "s/NETWORKADDR/${LAN[NETWORK]}/g" /etc/sysconfig/network-scripts/ifcfg-${lan_if}
sed -i "s/NETWORKADDR/${WLAN[NETWORK]}/g" /etc/sysconfig/network-scripts/ifcfg-${wlan_if}

sed -i "s/CIDRV/${EXTERNAL[PREFIX]}/g" /etc/sysconfig/network-scripts/ifcfg-${ext_if}
sed -i "s/CIDRV/${LAN[PREFIX]}/g" /etc/sysconfig/network-scripts/ifcfg-${lan_if}
sed -i "s/CIDRV/${WLAN[PREFIX]}/g" /etc/sysconfig/network-scripts/ifcfg-${wlan_if}

sed -i "s/HOSTV/${EXTERNAL[HOST]}/g" /etc/sysconfig/network-scripts/ifcfg-${ext_if}
sed -i "s/HOSTV/${LAN[HOST]}/g" /etc/sysconfig/network-scripts/ifcfg-${lan_if}
sed -i "s/HOSTV/${WLAN[HOST]}/g" /etc/sysconfig/network-scripts/ifcfg-${wlan_if}

cat >> /etc/sysconfig/network-scripts/ifcfg-${wlan_if} << EOF

GATEWAY=10.16.255.254
DNS1=142.232.221.253
DNS2=127.0.0.1

EOF

#Wiresless Configurations
cat >> /etc/sysconfig/network-scripts/ifcfg-${wlan_if} << EOF
ESSID=NASP19_${studentid}
CHANNEL=$wlan_channel
MODE=$wlan_mode
TYPE=$wlan_type
EOF

systemctl start network.service && systemctl restart network.service && systemctl enable network.service 2> /var/log/router-${cdate} > /dev/null
