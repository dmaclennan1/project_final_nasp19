#!/bin/bash
#DHCP Daemon Installation
source ~/project_final_nasp19/router/config.sh
source ~/project_final_nasp19/router/arrays.sh

echo "Starting dhcpd.service installation"
yum list installed dhcpd 2> /var/log/router-${cdate} > /dev/null

if [ $? == 1 ]; then
yum install -y dhcp 2> /var/log/router-${cdate} > /dev/null

cp -f ~/project_final_nasp19/router/services-conf/dhcpd.service/dhcpd.template /etc/dhcp/dhcpd.conf
sed -i "s/'DHCPSID'/$studentid/g" /etc/dhcp/dhcpd.conf
sed -i "s/'LANMASK'/$lannetmask/g" /etc/dhcp/dhcpd.conf
sed -i "s/'WLANMASK'/$wlannetmask/g" /etc/dhcp/dhcpd.conf
sed -i "s/'MAILMAC'/$mailmacaddr/g" /etc/dhcp/dhcpd.conf

echo "Dhcpd.service configured!"
echo "Starting dhcp.services.."
systemctl start dhcpd && systemctl restart dhcpd 2> /var/log/router-${cdate} > /dev/null
echo "Enabling dhcp.services for on-boot operation.."
systemctl enable dhcpd 2> /var/log/router-${cdate} > /dev/null

else

echo "Dhcpd.service already installed."
echo "Configuring dhcpd.service based on provided configuration"

cp -f ~/project_final_nasp19/router/services-conf/dhcpd.service/dhcpd.template  /etc/dhcp/dhcpd.conf
sed -i "s/'DHCPSID'/$studentid/g" /etc/dhcp/dhcpd.conf
sed -i "s/'LANMASK'/$lannetmask/g" /etc/dhcp/dhcpd.conf
sed -i "s/'WLANMASK'/$wlannetmask/g" /etc/dhcp/dhcpd.conf
sed -i "s/'MAILMAC'/$mailmacaddr/g" /etc/dhcp/dhcpd.conf

echo "Ddcpd.service configured"
echo "Starting dhcp.services.."
systemctl start dhcpd && systemctl restart dhcpd 2> /var/log/router-${cdate} > /dev/null
echo "Enabling dhcp.services for on-boot operation.."
systemctl enable dhcpd 2> /var/log/router-${cdate} > /dev/null
fi
