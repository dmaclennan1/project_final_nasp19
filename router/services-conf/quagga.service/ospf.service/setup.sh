#!/bin/bash
###################################################
# ---------- #OSPF Setup# ---------- #
###################################################
#DESIGNED FOR THE NASP 19 ROUTER CURRICULUM
source ~/project_final_nasp19/router/config.sh
source ~/project_final_nasp19/router/arrays.sh

echo "Starting installation of OSPF daemon.."

cp -f ~/project_final_nasp19/router/services-conf/quagga.service/ospf.service/ospf.template /etc/quagga/ospfd.conf
chown quagga:quagga /etc/quagga/ospfd.conf

echo "Ospf config file created.."
echo "Configuring ospf config file.."

sed -i "s/'OSPFWLANIF'/${WLAN[INTERFACE]}/g" /etc/quagga/ospfd.conf
sed -i "s/'OSPFLANIF'/${LAN[INTERFACE]}/g" /etc/quagga/ospfd.conf
sed -i "s/'OSPFEXIF'/${EXTERNAL[INTERFACE]}/g" /etc/quagga/ospfd.conf

sed -i "s/'OSPFWLANPFX'/${WLAN[PREFIX]}/g" /etc/quagga/ospfd.conf
sed -i "s/'OSPFLANPFX'/${LAN[PREFIX]}/g" /etc/quagga/ospfd.conf
sed -i "s/'OSPFEXTPFX'/${EXTERNAL[PREFIX]}/g" /etc/quagga/ospfd.conf

sed -i "s/'EXTOSPF'/${EXTERNAL[HOST]}/g" /etc/quagga/ospfd.conf

sed -i "s/'OSPFWLANNET'/${WLAN[NETWORK]}/g" /etc/quagga/ospfd.conf
sed -i "s/'OSPFLANNET'/${LAN[NETWORK]}/g" /etc/quagga/ospfd.conf
sed -i "s/'OSPFEXTNET'/${EXTERNAL[NETWORK]}/g" /etc/quagga/ospfd.conf

echo "Config file configured.."

echo "Starting ospfd.service.."
systemctl start ospfd.service && systemctl restart ospfd.service 2> /var/log/router-${cdate} > /dev/null

if [[ $? == 9 ]]; then
  echo "Ospfd.service has failed.."
  exit
else

echo "Enabling ospfd.service to start on boot.."
systemctl enable ospfd.service 2> /var/log/router-${cdate} > /dev/null
fi
