#!/bin/bash
###################################################
# ---------- #Zebra Setup# ---------- #
###################################################
#DESIGNED FOR THE NASP 19 ROUTER CURRICULUM
source ~/project_final_nasp19/router/config.sh
source ~/project_final_nasp19/router/arrays.sh

echo "Starting installation of Zebra daemon.."

echo "Re-creating zebra config file.."

cp -f ~/project_final_nasp19/router/services-conf/quagga.service/zebra.service/zebra.template /etc/quagga/zebra.conf
chown quagga:quagga /etc/quagga/zebra.conf

echo "Zebra config file re-created.."
echo "Configuring zebra config file.."

sed -i "s/'ZHOSTNAME'/rtr.s${studentid}.as.learn/g" /etc/quagga/zebra.conf
sed -i "s/'ZWLANIF'/${WLAN[INTERFACE]}/g" /etc/quagga/zebra.conf
sed -i "s/'ZLANIF'/${LAN[INTERFACE]}/g" /etc/quagga/zebra.conf
sed -i "s/'ZEXTIF'/${EXTERNAL[INTERFACE]}/g" /etc/quagga/zebra.conf

sed -i "s/'ZWLANPFX'/${WLAN[PREFIX]}/g" /etc/quagga/zebra.conf
sed -i "s/'ZLANPFX'/${LAN[PREFIX]}/g" /etc/quagga/zebra.conf
sed -i "s/'ZEXTPFX'/${EXTERNAL[PREFIX]}/g" /etc/quagga/zebra.conf

sed -i "s/'ZWLANHOST'/${WLAN[HOST]}/g" /etc/quagga/zebra.conf
sed -i "s/'ZLANHOST'/${LAN[HOST]}/g" /etc/quagga/zebra.conf
sed -i "s/'ZEXTHOST'/${EXTERNAL[HOST]}/g" /etc/quagga/zebra.conf

echo "Config file configured.."

echo "Starting zebra.service.."
systemctl start zebra.service && systemctl start zebra.service 2> /var/log/router-${cdate} > /dev/null

if [[ $? == 9 ]]; then
  echo "Zebra.service has failed to start.."
  exit
else

echo "Enabling zebra.service to start on boot.."
systemctl enable zebra.service 2> /var/log/router-${cdate} > /dev/null

fi
