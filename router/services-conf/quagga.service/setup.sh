#!/bin/bash
###################################################
# ---------- #Quagga Setup# ---------- #
source ~/project_final_nasp19/router/config.sh
source ~/project_final_nasp19/router/arrays.sh

echo "Starting quagga.service installation.."
yum list installed quagga 2> /var/log/router-${cdate} > /dev/null

if [ $? == 1 ]; then
yum install -y quagga 2> /var/log/router-${cdate} > /dev/null

echo "Quagga installed.."
echo "Starting ospf and zebra installation.."

else

  echo "Quagga already installed."
  echo "Starting ospf and zebra installation..."

fi

echo 1 > /proc/sys/net/ipv4/ip_forward
echo "IP Fowarding enabled."
