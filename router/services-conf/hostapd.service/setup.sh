#!/bin/bash
###################################################
# ---------- #HostAPD Setup# ---------- #
###################################################
source ~/project_final_nasp19/router/config.sh
source ~/project_final_nasp19/router/arrays.sh

echo "Starting hostapd.service installation"
yum list installed hostapd 2> /var/log/router-${cdate} > /dev/null

if [ $? == 1 ]; then
yum install -y hostapd 2> /var/log/router-${cdate} > /dev/null

echo "Configuring hostapd.."

cp -f ~/project_final_nasp19/router/services-conf/hostapd.service/hostapd.template /etc/hostapd/hostapd.conf

sed -i "s/'HOSTIF'/$hapif/" /etc/hostapd/hostapd.conf
sed -i "s/'HOSTDRIVER'/$hapdriver/" /etc/hostapd/hostapd.conf
sed -i "s/'HOSTESSID'/$hapessid/" /etc/hostapd/hostapd.conf
sed -i "s/'HOSTMODE'/$hapmode/" /etc/hostapd/hostapd.conf
sed -i "s/'HOSTCHANNEL'/$hapchannel/" /etc/hostapd/hostapd.conf
sed -i "s/'HOSTAUTH'/$hauth/" /etc/hostapd/hostapd.conf

echo "Hostapd configured!"
echo "Starting hostapd services."
echo "Enabling hostapd services to start on boot."

else

echo "Hostapd already installed. configuring hostapd.service based on provided configuration"

cp -f ~/project_final_nasp19/router/services-conf/hostapd.service/hostapd.template /etc/hostapd/hostapd.conf

sed -i "s/'HOSTIF'/$hapif/" /etc/hostapd/hostapd.conf
sed -i "s/'HOSTDRIVER'/$hapdriver/" /etc/hostapd/hostapd.conf
sed -i "s/'HOSTESSID'/$hapessid/" /etc/hostapd/hostapd.conf
sed -i "s/'HOSTMODE'/$hapmode/" /etc/hostapd/hostapd.conf
sed -i "s/'HOSTCHANNEL'/$hapchannel/" /etc/hostapd/hostapd.conf
sed -i "s/'HOSTAUTH'/$hauth/" /etc/hostapd/hostapd.conf

echo "Hostapd configured!"
fi

echo "Starting hostapd services.."
systemctl start hostapd && systemctl restart hostapd 2> /var/log/router-${cdate} > /dev/null
if [[ $? == 1 ]]; then
  echo "Hostapd.service has failed to start"
  exit

else

echo "Enabling hostapd.service to start on boot.."
systemctl enable hostapd 2> /var/log/router-${cdate} > /dev/null

fi
