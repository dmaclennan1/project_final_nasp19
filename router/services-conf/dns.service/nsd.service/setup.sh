#!/bin/bash
###################################################
# ---------- #NSD Setup# ---------- #
###################################################

source ~/project_final_nasp19/router/config.sh
source ~/project_final_nasp19/router/arrays.sh

echo "Installing nsd daemon"
yum install -y nsd > /dev/null

echo "Configuring nsd daemon config file"

cp ~/project_final_nasp19/router/services-conf/dns.service/nsd.service/nsd.template /etc/nsd/nsd.conf

sed -i "s/'nsdface'/$nsdint/g" /etc/nsd/nsd.conf

sed -i "s/'ip4listen'/$ipv4listen/g" /etc/nsd/nsd.conf
sed -i "s/'ip6listen'/$ipv6listen/g" /etc/nsd/nsd.conf

sed -i "s/'nsdport'/$nsdport/g" /etc/nsd/nsd.conf

sed -i "s/'ZONE1A'/${ZONES1[A]}/g" /etc/nsd/nsd.conf
sed -i "s/'ZONE1PTR'/${ZONES1[PTR]}/g" /etc/nsd/nsd.conf

echo "Nsd.service config file is complete"
echo "Configuring nsd.service zones.."
echo "Configuring zone files"

cp ~/project_final_nasp19/router/services-conf/dns.service/nsd.service/zones/hostzone1 /etc/nsd/${ZONES1[A]}.zone
cp ~/project_final_nasp19/router/services-conf/dns.service/nsd.service/zones/ptrzone1 /etc/nsd/${ZONES1[PTR]}.zone

sed -i "s/'STUDENTID'/${studentid}/g" /etc/nsd/${ZONES1[PTR]}.zone
sed -i "s/'STUDENTID'/${studentid}/g" /etc/nsd/${ZONES1[A]}.zone

sed -i "s/'MAILHOST'/${mail_addr}/g" /etc/nsd/${ZONES1[A]}.zone
sed -i "s/'EXTHOST'/${EXTERNAL[HOST]}/g" /etc/nsd/${ZONES1[A]}.zone

echo "Zone files configured"
echo "Starting nsd.service."
systemctl start nsd.service && systemctl restart nsd.service 2> /var/log/router-${cdate} > /dev/null
if [[ $? == 9 ]]; then
  echo "Nsd.service has failed to start"
  exit
else

echo "Enabling nsd.service to start on boot."
systemctl enable nsd.service 2> /var/log/router-${cdate} > /dev/null

fi
