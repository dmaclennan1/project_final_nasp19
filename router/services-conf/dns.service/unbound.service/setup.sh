#!/bin/bash
###################################################
# ---------- #Unbound Setup          # ---------- #
###################################################

source ~/project_final_nasp19/router/config.sh
source ~/project_final_nasp19/router/arrays.sh

echo "Installing unbound services."
yum install -y unbound > /dev/null
echo "Unbound service installed"

echo "Installing root.hints"
wget http://www.internic.net/domain/named.root
mv -f named.root /etc/unbound/root.hints
echo "root.hints installed"

echo "Creating unbound.conf file"
cp -f ~/project_final_nasp19/router/services-conf/dns.service/unbound.service/unbound.template /etc/unbound/unbound.conf
echo "unbound.conf file created"

echo "Configuring unbound config file"
#LISTEN OPTIONS

sed -i "s/'UNBOUNDINT'/$ubface/g" /etc/unbound/unbound.conf
sed -i "s/'unboundport'/$unboundport/g" /etc/unbound/unbound.conf

#IPV ACCESS
sed -i "s/'ip4vote'/$ipv4access/g" /etc/unbound/unbound.conf
sed -i "s/'ip6vote'/$ipv6access/g" /etc/unbound/unbound.conf

#INTERFACE ACCESS
sed -i "s/access-control:\ 'extnet'\/'extpfx'\ 'extallow'/access-control:\ ${EXTERNAL[NETWORK]}\/${EXTERNAL[PREFIX]}\ $wlanaccess/g" /etc/unbound/unbound.conf
sed -i "s/access-control:\ 'lannet'\/'lanpfx'\ 'lanallow'/access-control:\ ${LAN[NETWORK]}\/${LAN[PREFIX]}\ $lanaccess/g" /etc/unbound/unbound.conf
sed -i "s/access-control:\ 'wlannet'\/'wlanpfx'\ 'wlanallow'/access-control:\ ${WLAN[NETWORK]}\/${WLAN[PREFIX]}\ $extaccess/g" /etc/unbound/unbound.conf


#MODULE SETTING
sed -i "s/'unboundmodcon'/$moduleconfig/g" /etc/unbound/unbound.conf

#STUB ZONES
sed -i "s/'stubasdomain'/${stubzone1[FW]}/g" /etc/unbound/unbound.conf
sed -i "s/'stubptr'/${stubzone1[PTR]}/g" /etc/unbound/unbound.conf
sed -i "s/'exthost'/${EXTERNAL[HOST]}/g" /etc/unbound/unbound.conf

#FORWARD ZONES
sed -i "s/'forward1'/${ubforward[1]}/g" /etc/unbound/unbound.conf
sed -i "s/'forward2'/${ubforward[2]}/g" /etc/unbound/unbound.conf
sed -i "s/'forward3'/${ubforward[3]}/g" /etc/unbound/unbound.conf
sed -i "s/'forward_address1'/${ubforaddr[1]}/g" /etc/unbound/unbound.conf
sed -i "s/'forward_address2'/${ubforaddr[2]}/g" /etc/unbound/unbound.conf
sed -i "s/'forward_address3'/${ubforaddr[3]}/g" /etc/unbound/unbound.conf
sed -i "s/'forward_address4'/${ubforaddr[4]}/g" /etc/unbound/unbound.conf

echo "Unbound config file configured"
echo "Start unbound.service"
systemctl restart unbound.service 2> /var/log/router-${cdate} > /dev/null

if [[ $? == 9 ]]; then
  echo "Unbound.service has failed to start"
  exit
else

echo "Enabling unbound to load on boot"
systemctl enable unbound.service 2> /var/log/router-${cdate} > /dev/null

fi
