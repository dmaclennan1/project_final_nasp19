#!/binbash
###################################################
#        CONFIGURATION FILE FOR MAIL              #
#      #-----EDIT AS NECESSARY-----#              #
###################################################

###################################################
#  --------#Preconfiguration Variables# --------  #
###################################################
declare cdate=`date +%Y-%m-%d:%H:%M`
declare studentnumber='12'
hostnamectl set-hostname mail.s${studentnumber}.as.nasp
declare interfacename='eth0'

###################################################
#  -------------#Postfix Variables# ------------  #
###################################################

declare postfixint='all'
declare postfixdest='3'
declare postfixmail='2'

###################################################
#  -------------#Dovecot Variables# ------------  #
###################################################

declare doveproto=' imap pop3 '
declare dovemail='1'
declare doveauthpt='no'

###################################################
#  ----------#Encryption Variables# ------------  #
###################################################

declare saslencryption='yes'
declare encrypproto=' imap imaps pop3 pop3s '
