#!/bin/bash
source ~/project_final_nasp19/mail/config.sh

yum install -y postfix > /dev/null 2> /var/log/mail-${cdate} 2> /var/log/mail-${cdate} > /dev/nulll

cp -f ~/project_final_nasp19/mail/services.conf/postfix.service/postfix.template /etc/postfix/main.cf

sed -i 's/#inet_interfaces = localhost/inet_interfaces = all/g' /etc/postfix/main.cf

sed -i 's/#home_mailbox = Maildir/home_mailbox = Maildir/g' /etc/postfix/main.cf

sed -i 's/#mydestination = $myhostname, localhost.$mydomain, localhost, $mydomain/mydestination = $myhostname, localhost.$mydomain, localhost, $mydomain/g' /etc/postfix/main.cf

systemctl restart postfix.service
