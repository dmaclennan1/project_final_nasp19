#!/bin/bash

source ~/project_final_nasp19/mail/config.sh

yum install -y dovecot

cp -f ~/project_final_nasp19/mail/services.conf/dovecot.service/dovecot.template /etc/dovecot/dovecot.conff

sed -i "s/#protocols = 'doveprot'/protocols = $doveproto/g" /etc/dovecot/dovecot.conf


cp -f ~/project_final_nasp19/mail/services.conf/dovecot.service/10-mail.template /etc/dovecot/conf.d/10-mail.conf

if [[ $dovemail == 1 ]]; then
sed -i "s/#1//g" /etc/dovecot/conf.d/10-mail.conf
else
  echo '' > /dev/null
fi

if [[ $dovemail == 2 ]]; then
sed -i "s/#2//g" /etc/dovecot/conf.d/10-mail.conf
else
  echo '' > /dev/null
fi

if [[ $dovemail == 3 ]]; then
sed -i "s/#3//g" /etc/dovecot/conf.d/10-mail.conf
else
  echo '' > /dev/null
fi

if [[ $dovemail == 4 ]]; then
sed -i "s/#mail_location/mail_location = $dovemail/g" /etc/dovecot/conf.d/10-mail.conf
else
  echo '' > /dev/null
fi

cp -f ~/project_final_nasp19/mail/services.conf/dovecot.service/10-auth.template /etc/dovecot/conf.d/10-auth.conf

sed -i "s/'dovecotpt'/$doveauthpt/g" /etc/dovecot/conf.d/10-auth.conf

echo "Starting dovecot.service.."
systemctl restart dovecot

echo "Enabling dovecot.service.."
