#!/bin/bash
source ~/project_final_nasp19/mail/config.sh

echo "Setting up dovecot.service and postfix.service for SASL encryption"

if [[ $saslencryption == yes ]]; then

cat >> /etc/postfix/main.cf << EOF

#SASL ADDITIONS
smtpd_sasl_auth_enable = yes
broken_sasl_auth_clients = yes
smtpd_sasl_type = dovecot
smtpd_sasl_path = private/auth
smtpd_sasl_security_options = noanonymous
smtpd_recipient_restrictions =
    permit_mynetworks,
    permit_sasl_authenticated,
    reject_unauth_destination

  EOF

  cat >> /etc/dovecot/dovecot.conf << EOF

#SASL ADDITIONS
auth default {
    mechanisms = plain login
    passdb pam {
    }
    userdb passwd {
    }
    user = root
    socket listen {
      client {
        path = /var/spool/postfix/private/auth
        mode = 0660
        user = postfix
        group = postfix
      }
    }
}

EOF

else

  exit

fi

echo "Setup complete.."

echo "Restarting dovecot and postfix services.."

systemctl restart dovecot.service && systemctl restart postfix.service

echo "Services restarted..."

echo "Configuring encryption.."

genkey --days 365 mail.nasp${studentnumber}.as.learn

cat >> /etc/postfix/main.cf << EOF

smtpd_tls_security_level = may
smtpd_tls_key_file = /etc/pki/tls/private/mail.nasp${studentnumber}.as.learn.cert
smtpd_tls_cert_file = /etc/pki/tls/certs/mail.nasp${studentnumber}.as.learn.key
# smtpd_tls_CAfile = /etc/pki/tls/root.crt
smtpd_tls_loglevel = 1
smtpd_tls_session_cache_timeout = 3600s
smtpd_tls_session_cache_database = btree:/var/lib/postfix/smtpd_tls_cache.0
tls_random_source = dev:/dev/urandom
tls_random_exchange_name = /var/lib/postfix/prng_exch
smtpd_tls_auth_only = yes

EOF

cat >> /etc/dovecot/dovecot.conf << EOF

protocols = imap imaps pop3 pop3s
#disable_plaintext_auth = no
#ssl_disable = no
ssl_cert_file = /etc/pki/tls/certs/mail.nasp${studentnumber}.as.learn.cert
ssl_key_file = /etc/pki/tls/private/mail.nasp${studentnumber}.as.learn.key
ssl_cipher_list = ALL:!LOW:!SSLv2

EOF

echo "Encryption successfully configured, restarting services."

systemctl restart dovecot.service && systemctl restart postfix.service

echo "Services restarted..."
