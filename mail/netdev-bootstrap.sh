#!/bin/bash -
set -o nounset                              # Treat unset variables as an error
source ~/project_final_nasp19/mail/config.sh
echo "Installing base packages"
yum -y update 2> /var/log/mail-${cdate} > /dev/null
echo "group_package_types=mandatory,default,optional" >> /etc/yum.conf
yum -y group install base 2> /var/log/mail-${cdate} > /dev/null

echo "Installing the Extra Packages for Enterprise Linux Repository"
yum -y install epel-release 2> /var/log/mail-${cdate} > /dev/null
yum -y update > /dev/null  2> /var/log/mail-${cdate} > /dev/null

echo "Installing project specific tools"
yum -y install curl vim wget tmux nmap-ncat tcpdump nmap git perl crypto-utils mod_ssl 2> /var/log/mail-${cdate} > /dev/nulll

echo "Setting Up VirtualBox Guest Additions"
echo "Installing pre-requisities"
yum -y install kernel-devel kernel-headers dkms gcc gcc-c+ 2> /var/log/mail-${cdate} > /dev/null

echo "Creating mount point, mounting, and installing VirtualBox Guest Additions"
mkdir vbox_cd 2> /var/log/mail-${cdate} > /dev/null
mount /dev/cdrom ./vbox_cd 2> /var/log/mail-${cdate} > /dev/null
./vbox_cd/VBoxLinuxAdditions.run 2> /var/log/mail-${cdate} > /dev/null
umount ./vbox_cd 2> /var/log/mail-${cdate} > /dev/null
rmdir ./vbox_cd 2> /var/log/mail-${cdate} > /dev/null

echo "Disabling selinux"
setenforce 0
sed -r -i 's/SELINUX=(enforcing|disabled)/SELINUX=permissive/' /etc/selinux/config 2> /var/log/mail-${cdate} > /dev/null

echo "Turning off and disabling Network Manager"
systemctl stop NetworkManager.service 2> /var/log/mail-${cdate} > /dev/null
systemctl disable NetworkManager.service 2> /var/log/mail-${cdate} > /dev/null

echo "Turning off and disabling Firewall Daemon"
systemctl stop firewalld.service 2> /var/log/mail-${cdate} > /dev/null
systemctl disable firewalld.service 2> /var/log/mail-${cdate} > /dev/null

echo "Enabling and starting the Network Service (ignoring angry messages)"
systemctl enable network.service 2> /var/log/mail-${cdate} > /dev/null
systemctl start network.service 2> /var/log/mail-${cdate} > /dev/null

echo "Enable sshd"
systemctl enable sshd.service 2> /var/log/mail-${cdate} > /dev/null
systemctl start sshd.service 2> /var/log/mail-${cdate} > /dev/null

echo "Setting up Instructor User"
useradd -m -G wheel,users instructor 2> /var/log/mail-${cdate} > /dev/null

if [[ $? == 9 ]]; then
echo "Instructor user already exists, ignoring instructor installation"
exit

else

echo "instructor ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers
mkdir ~instructor/.ssh/ 2> /var/log/mail-${cdate} > /dev/null
cat > ~instructor/.ssh/authorized_keys <<EOF
ecdsa-sha2-nistp521 AAAAE2VjZHNhLXNoYTItbmlzdHA1MjEAAAAIbmlzdHA1MjEAAACFBACbbA/5CA4Z5AhmOX4JMyxqXIh7JwR7B6S0DOFCj4k8Y8255K/bGXWw5tFWokSCi+89wnj7Y5AIrEnhMo9Pp2y3iQG21hFs+Ba0KI7cSL73X4bUBhLy1EUZjo5wNcPTNG1YgG94a9iTIoqUtoZLRiDvmPMvNR929dOTD5UEA3t3wy2XXg== nasp@milkplus
EOF
chown -R instructor:instructor ~instructor/.ssh

fi
