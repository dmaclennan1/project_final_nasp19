#!/bin/bash

source ~/project_final_nasp19/mail/config.sh
echo "Starting mail server configuration.."
bash ~/project_final_nasp19/mail/config.sh

echo "Creating log file in /var/log/mail-${cdate}"
touch /var/log/mail-${cdate}

echo "Configuring network interfaces.."
bash ~/project_final_nasp19/mail/interface.conf/setup.sh
echo "Network interfaces configured.."

echo "Installing netdev-bootstrap.."
bash  ~/project_final_nasp19/mail/netdev-bootstrap.sh
echo "Boostrap installed.."

echo "Installing postfix.service.. "
bash ~/project_final_nasp19/mail/services.conf/postfix.service/setup.sh
echo "Postfix.service installed..."

echo "Installing dovecot.service.."
bash ~/project_final_nasp19/mail/services.conf/dovecot.service/setup.sh
echo "Dovecot.service installed.."

#echo "Configuring SASL/SSL encryption for dovecot.service and postfix.service.."
#bash ~/project_final_nasp19/mail/services.conf/encryption.service/setup.sh
#echo "Encyption configured.."

echo "Mail server configurtion complete.."
