#!/bin/bash

echo "Student A00971463's Router/Mail Configuration Script"
echo "Syntax is ./main.sh <mail | router>"
echo "Please note that SASL/SSL encryption is not configured"

arg=$@

case arg in
  router)
  echo "Router confiugration has been selected"
  bash ~/project_final_nasp19/router/main.sh
;;
  mail)
  echo "Mail confiugration has been selected"
  bash ~/project_final_nasp19/mail/main.sh
;;
  *)
  echo "Incorrect syntax used. Valid options are <mail | router>"
  exit
;;
esac
